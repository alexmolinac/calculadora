struct entradas{
	float num1;
	float num2;
	char operador;
};

program CALCULADORA_PROG{
	version CALCULADORA_VER{
		float SUM(entradas)=1;
		float RES(entradas)=2;
		float MUL(entradas)=3;
		float DIV(entradas)=4;
		float RAIZ(entradas)=5;
		float EXP(entradas)=6;
	}=1;
}=0x2fffffff;
